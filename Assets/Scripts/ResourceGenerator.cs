using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceGenerator : MonoBehaviour
{
    private BuildingTypeSO _buildingType;
    private float timer;
    private float timerMax;

    private void Awake()
    {
        _buildingType = GetComponent<BuildingTypeHolder>().buildingType;
        // foreach (var resourceGeneratorData in _buildingType.ResourceGeneratorDataList)
        // {
        //     timerMax = resourceGeneratorData.timerMax;
        // }
        timerMax = _buildingType.resourceGeneratorData.timerMax;
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0f)
        {
            timer += timerMax;
            // foreach (var generatorData in _buildingType.ResourceGeneratorDataList)
            // {
            //     ResourceManager.Instance.AddResourceType(generatorData.resourceType, 1);
            // }
            ResourceManager.Instance.AddResourceType(_buildingType.resourceGeneratorData.resourceType, 1);
        }
    }
}
