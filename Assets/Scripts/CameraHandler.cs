using Cinemachine;
using UnityEngine;
using UnityEngine.Serialization;

public class CameraHandler : MonoBehaviour
{
    [SerializeField] private float moveSpeed;

    [SerializeField]
    private CinemachineVirtualCamera cinemachineVirtualCamera;

    [SerializeField]
    private float zoomAmount = 2;

    [SerializeField]
    private float zoomSpeed = 5;

    [SerializeField]
    private float minOrthographicSize = 10;

    [SerializeField]
    private float maxOrthographicSize = 30;

    private float _orthoGraphicSize;
    private float _targetOrthographicSize;

    void Start()
    {
        _orthoGraphicSize = cinemachineVirtualCamera.m_Lens.OrthographicSize;
        _targetOrthographicSize = _orthoGraphicSize;
    }

    void Update()
    {
        HandleMovement();
        HandleZoom();
    }

    private void HandleMovement()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        // this needs to be a Vector3 as position is a Vector3
        Vector3 moveDirection = new Vector3(x, y, 0).normalized;
        transform.position += moveDirection * (moveSpeed * Time.deltaTime);
    }

    private void HandleZoom()
    {
        _targetOrthographicSize -= Input.mouseScrollDelta.y * zoomAmount;
        Debug.Log(_targetOrthographicSize);

        // sets min/max zoom
        _targetOrthographicSize = Mathf.Clamp(_targetOrthographicSize, minOrthographicSize, maxOrthographicSize);

        _orthoGraphicSize = Mathf.Lerp(_orthoGraphicSize, _targetOrthographicSize, Time.deltaTime * zoomSpeed);
        
        cinemachineVirtualCamera.m_Lens.OrthographicSize = _orthoGraphicSize;
    }
}
