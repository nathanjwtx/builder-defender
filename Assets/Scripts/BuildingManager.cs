using System;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    [SerializeField]
    private BuildingTypeSO buildingType;
    
    private Camera mainCamera;
    private BuildingTypeListSO _buildingTypeListSo;
    private BuildingTypeSO _buildingTypeSo;

    private void Awake()
    {
        // Resources is external but generated at compile time so will be available for the script
        _buildingTypeListSo = Resources.Load<BuildingTypeListSO>(typeof(BuildingTypeListSO).Name);
        _buildingTypeSo = _buildingTypeListSo.buildingTypes[0];
    }

    private void Start()
    {
        // we could use Awake() here but Camera is an external/scene object and may not be loaded in when Awake is called. Awake runs before Start
        mainCamera = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(buildingType.prefab, GetMouseWorldPosition(), Quaternion.identity);
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            buildingType = _buildingTypeSo; // use the default building type set above
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            buildingType = _buildingTypeListSo.buildingTypes[1];
        }
    }

    private Vector3 GetMouseWorldPosition()
    {
        Vector3 mouseWorldPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPosition.z = 0f;
        return mouseWorldPosition;
    }
}
