using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResourcesUI : MonoBehaviour
{
    private ResourceTypeListSO _resourceTypeList;
    private Dictionary<string, Transform> _resourceTextMeshTransform = new Dictionary<string, Transform>();
    private void Awake()
    {
        _resourceTypeList = Resources.Load<ResourceTypeListSO>(typeof(ResourceTypeListSO).Name);
        // ResourceTypeListSO resourceTypeList = Resources.Load<ResourceTypeListSO>(typeof(ResourceTypeListSO).Name);

        // Transform resourceTemplate = transform.Find("ResourceTemplate"); // could also Serialize the field and set it in the editor
        // resourceTemplate.gameObject.SetActive(false);

        // int index = 0;
        // foreach (var resourceType in resourceTypeList.resourceTypes)
        // {
            // Transform resourceTransform = Instantiate(resourceTemplate, transform);
            // resourceTransform.gameObject.SetActive(true);
            // float offsetAmount = -160f;
            // resourceTransform.GetComponent<RectTransform>().anchoredPosition = new Vector2(offsetAmount * index, 0);
            // resourceTransform.Find("Image").GetComponent<Image>().sprite = resourceType.sprite;

            // int resourceAmount = ResourceManager.Instance.GetResourceAmount(resourceType);
            // resourceTransform.Find("Text").GetComponent<TextMeshProUGUI>().SetText(resourceAmount.ToString());
            
            // index++;
        // }
    }

    private void Start()
    {
        // ResourceManager.Instance.OnResourceAmountChanged += ResourceManager_OnResourceAmountChanged;
        ResourceManager.Instance.OnResourceAmountChanged += (sender, resourceAmountChangedArgs) => 
        {
            UpdateResourceAmount(resourceAmountChangedArgs.ResourceType);
        };
        InitialResourceAmount();
    }

    // private void ResourceManager_OnResourceAmountChanged(object sender, ResourceAmountChangedArgs e)
    // {
    //     Debug.Log($"Resource: {e.ResourceType.nameString}");
    //     UpdateResourceAmount(e.ResourceType);
    // }

    private void InitialResourceAmount()
    {
        Transform resourceTemplate = transform.Find("ResourceTemplate"); // could also Serialize the field and set it in the editor
        resourceTemplate.gameObject.SetActive(false);
        
        int index = 1;
        foreach (var resourceType in _resourceTypeList.resourceTypes)
        {
            Transform resourceTransform = Instantiate(resourceTemplate, transform);
            resourceTransform.gameObject.SetActive(true);
            
            _resourceTextMeshTransform.Add(resourceType.nameString, resourceTransform);
            
            float offsetAmount = -160f;
            resourceTransform.GetComponent<RectTransform>().anchoredPosition = new Vector2(offsetAmount * index, -100);
            Debug.Log(resourceType.nameString);
            resourceTransform.Find("Image").GetComponent<Image>().sprite = resourceType.sprite;

            // int resourceAmount = ResourceManager.Instance.GetResourceAmount(resourceType);
            // resourceTransform.Find("Text").GetComponent<TextMeshProUGUI>().SetText(resourceAmount.ToString());
            UpdateResourceAmount(resourceType);

            index++;
        }
    }

    private void UpdateResourceAmount(ResourceTypeSO resourceType)
    {
            int resourceAmount = ResourceManager.Instance.GetResourceAmount(resourceType);
            var resourceTransform = _resourceTextMeshTransform[resourceType.nameString];
            resourceTransform.Find("Text").GetComponent<TextMeshProUGUI>().SetText(resourceAmount.ToString());
    }
}
