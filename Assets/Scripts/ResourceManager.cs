using System;
using System.Collections.Generic;
using UnityEngine;

public class ResourceAmountChangedArgs : EventArgs
{
    public ResourceAmountChangedArgs(ResourceTypeSO resourceType)
    {
        ResourceType = resourceType;
    }

    public ResourceTypeSO ResourceType { get; set; }
}

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager Instance { get; private set; }

    public event EventHandler<ResourceAmountChangedArgs> OnResourceAmountChanged;
    
    private Dictionary<ResourceTypeSO, int> resourceAmountDictionary;

    private void Awake()
    {
        Instance = this;
        
        resourceAmountDictionary = new Dictionary<ResourceTypeSO, int>();

        ResourceTypeListSO resourceTypeList = Resources.Load<ResourceTypeListSO>(typeof(ResourceTypeListSO).Name);

        foreach (ResourceTypeSO resourceType in resourceTypeList.resourceTypes)
        {
            resourceAmountDictionary[resourceType] = 0;
        }
        
        // TestLog();
    }

    public void AddResourceType(ResourceTypeSO resourceType, int resourceAmount)
    {
        Debug.Log(resourceAmount);
        resourceAmountDictionary[resourceType] += resourceAmount;
        
        OnRaiseResourceAmountChangeEvent(new ResourceAmountChangedArgs(resourceType));
        
        // TestLog();
    }

    protected virtual void OnRaiseResourceAmountChangeEvent(ResourceAmountChangedArgs e)
    {
        EventHandler<ResourceAmountChangedArgs> raisedEvent = OnResourceAmountChanged;

        raisedEvent?.Invoke(this, e);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            ResourceTypeListSO resourceTypeList = Resources.Load<ResourceTypeListSO>(typeof(ResourceTypeListSO).Name);
            AddResourceType(resourceTypeList.resourceTypes[0], 0);
        }
    }

    private void TestLog()
    {
        foreach (var resourcesType in resourceAmountDictionary.Keys)
        {
            Debug.Log($"{resourcesType.nameString}: {resourceAmountDictionary[resourcesType]}");
        }
    }

    public int GetResourceAmount(ResourceTypeSO resourceTypeSo)
    {
        return resourceAmountDictionary[resourceTypeSo];
    }
}
